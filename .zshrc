export MANPATH=$MANPATH:~/.bin/.man
export PATH=$PATH:~/.bin

zstyle ':completion:*' command-path ~/.bin

cd ~/.bin
git=$(git pull --dry-run 2>&1)
if ! [ "$git" = "" ]
then
	echo "You can update .bin. Are you ok? [Y/n]: \c"
	read Yn
	if [ "$Yn" = Y ] || [ "$Yn" = y ]
	then
		git pull
	else
		echo "no update"
	fi
fi
cd ~

# $1 file
# $2 var_name
# $3 value
function ini_write
{
	ini_delete $1 $2
	echo "$2=$3" >> $1
}

# $1 file
# $2 var_name
function ini_delete
{
	if ! [ -f $1 ]; then return 1; fi
	grep -v "^$2[ 	]*=" $1 2> /dev/null > $1
}

# $1 file
# $2 var_name
function ini_read
{
	if ! [ -f $1 ]; then return 1; fi
	val=$(grep "^$2[ 	]*=" $1 | wc -l | bc)
	if ! [ $val -eq 1 ]; then return 2; fi
	grep "^$2[ 	]*=" $1 | cut -d '=' -f 2 2> /dev/null
}

# var
function go
{
	if [ -z $1 ];
	then
		cd `ini_read ~/.ft_gosetrc "DEFAULT"`
	elif ini_read ~/.ft_gosetrc "$1" > /dev/null;
	then
		cd `ini_read ~/.ft_gosetrc "$1"`
	else
		echo "Path not found: \"$1\"" >&2
	fi
}

#echo "[`date`] $USER" | nc controleur.no-ip.org 9000

