#!/usr/bin/python

import sys
from optparse import OptionParser

CLASSHPP = """#ifndef <CLASS_MAJ>_HPP
#define <CLASS_MAJ>_HPP

class <CLASS> {

	private:

	protected:

	public:
		<CLASS>(void);
		~<CLASS>(void);
		<CLASS>(<CLASS> const &);

		<CLASS>		&operator=(<CLASS> const &);
};

#endif
"""

CLASSCPP = """#include "<CLASS>.hpp"

<CLASS>::<CLASS>(void)
{
}

<CLASS>::~<CLASS>(void)
{
}

<CLASS>::<CLASS>(<CLASS> const &o)
{
}

<CLASS>		&<CLASS>::operator=(<CLASS> const &o)
{
}
"""

if __name__ == "__main__":

	usage = "%prog [options] classname"
	parser = OptionParser(usage=usage)

	parser.add_option("--stdout",
						dest="stdout",
						default=False,
						action="store_true",
						help="print to stdout")

	parser.add_option("--hpp",
						dest="hpp",
						default=False,
						action="store_true",
						help="print only hpp")

	parser.add_option("--cpp",
						dest="cpp",
						default=False,
						action="store_true",
						help="print only cpp")

	(options, args) = parser.parse_args()
	if (len(args) != 1):
		parser.print_help()
		sys.exit(1)

	className = args[0]

	CLASSHPP = CLASSHPP.replace("<CLASS>", className)
	CLASSHPP = CLASSHPP.replace("<CLASS_MAJ>", className.upper())

	CLASSCPP = CLASSCPP.replace("<CLASS>", className)
	CLASSCPP = CLASSCPP.replace("<CLASS_MAJ>", className.upper())

	if (options.hpp == False and options.cpp == False):
		options.hpp = True
		options.cpp = True

	if (options.stdout):
		if (options.hpp):
			print CLASSHPP
		if (options.cpp):
			print CLASSCPP
	else:
		if (options.hpp):
			fileHpp = open(className + ".hpp", 'w')
			fileHpp.write(CLASSHPP)
			fileHpp.close()
		if (options.cpp):
			fileCpp = open(className + ".cpp", 'w')
			fileCpp.write(CLASSCPP)
			fileCpp.close()

