#!/bin/sh

# check dir ~/.bin exist
if [ -d ~/.bin ]
then
	echo "Error dir ~/.bin exist"
	echo "If you want remove this dir enter yes [y/n]: \c"
	read yn
	yn=`echo $yn | tr '[:upper:]' '[:lower:]'`
	if [ "$yn" = "yes" ] || [ "$yn" = "y" ] || [ "$yn" = "o" ] || [ "$yn" = "oui" ]; then
		rm -rf ~/.bin
	else
		exit 1
	fi
fi

# check ~/.zshrc exist
if ! [ -f ~/.zshrc ]
then
	echo "Not found ~/.zshrc"
	exit 1
fi

source=`grep "source ~/.bin/.zshrc" ~/.zshrc | wc -l | bc`
manpath=`grep "MANPATH=\$MANPATH:~/.bin/.man" ~/.zshrc | wc -l | bc`
git clone https://bitbucket.org/born2code42/bin.git ~/.bin

if [ $source -eq 0 ]
then
	echo "source ~/.bin/.zshrc" >> ~/.zshrc
fi

